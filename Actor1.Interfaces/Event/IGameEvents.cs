﻿using System;
using Microsoft.ServiceFabric.Actors;

namespace Actor1.Interfaces.Event
{
    public interface IGameEvents : IActorEvents
    {
        void GameScoreUpdated(Guid gameId, string currentScore);
    }
}
