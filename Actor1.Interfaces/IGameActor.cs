﻿using System.Threading.Tasks;
using Actor1.Interfaces.Event;
using Microsoft.ServiceFabric.Actors;

namespace Actor1.Interfaces
{
    public interface IGameActor : IActor, IActorEventPublisher<IGameEvents>
    {
        Task UpdateGameStatus(string status);

        Task<string> GetGameScore();
    }
}
