﻿using System;
using System.Threading.Tasks;
using Actor1.Interfaces;
using Actor1.Interfaces.Event;
using Microsoft.ServiceFabric.Actors;
using Microsoft.ServiceFabric.Actors.Runtime;

namespace Actor1
{
    [StatePersistence(StatePersistence.None)]
    internal class GameActor : Actor, IGameActor
    {
        /// <summary>
        /// 初始化 GameActor 的新实例
        /// </summary>
        ///  <param name="actorService">将托管此角色实例的 Microsoft.ServiceFabric.Actors.Runtime.ActorService。</param>
        /// <param name="actorId">此角色实例的 Microsoft.ServiceFabric.Actors.ActorId。</param>
        public GameActor(ActorService actorService, ActorId actorId)
            : base(actorService, actorId)
        {
        }

        /// <summary>
        /// 每当激活角色时，都会调用此方法。
        /// 首次调用任意角色方法时，都会激活角色。
        /// </summary>
        protected override Task OnActivateAsync()
        {
            ActorEventSource.Current.ActorMessage(this, "Actor activated.");
            return null;
        }

        Task<string> IGameActor.GetGameScore()
        {
            ActorEventSource.Current.ActorMessage(this, "Actor activated.");
            var ev = GetEvent<IGameEvents>();
            ActorId id = new ActorId("123456");
            ev.GameScoreUpdated(id.GetGuidId(), "222");
            return Task.Factory.StartNew(() =>
            {
                return "ok";
            });
        }

        Task IGameActor.UpdateGameStatus(string status)
        {
            ActorEventSource.Current.ActorMessage(this, status);
            return Task.Factory.StartNew(() =>
            {
                
            });
        }
    }
}
