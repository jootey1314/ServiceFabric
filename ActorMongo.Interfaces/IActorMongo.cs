﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.ServiceFabric.Actors;
using Models.Mongo;

namespace ActorMongo.Interfaces
{
    /// <summary>
    /// 此接口定义角色所公开的方法。
    /// 客户端使用此接口与实现它的角色进行交互。
    /// </summary>
    public interface IActorMongo : IActor
    {
        /// <summary>
        /// TODO: 替换为你自己的角色方法。
        /// </summary>
        /// <returns></returns>
        Task<int> GetCountAsync(CancellationToken cancellationToken);

        /// <summary>
        /// TODO: 替换为你自己的角色方法。
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        Task SetCountAsync(int count, CancellationToken cancellationToken);
        
        Task<string> AddAccountAsync(string name);

        Task<List<User>> GetAccountByNameAsync(string name);
    }
}
