﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ServiceFabric.Actors.Runtime;
using Models.Mongo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace ActorMongo.Service
{
    //连接串在Host的配置文件中
    public class AccountService : MongoDbProvider
    {
        public override string MongoDb => "ServiceFabric";
        public override string CollecitonName => "Order";
        public AccountService(ActorService actorService):base(actorService)
        {}
        public async Task<string> Save(string name)
        {
            await _Collection.InsertOneAsync(new BsonDocument { { "name", name }, { "_id", Guid.NewGuid().ToString() } });
            return "新增成功";
        }


        public async Task<List<User>> Get(string name)
        {
            FilterDefinition<BsonDocument> filter = new BsonDocument {{"name", name}};
            var result = await _Collection.FindAsync<User>(filter);
            return result.ToList();
        }

    }

    //[BsonIgnoreExtraElements]
    //public class User
    //{
    //    [BsonId]
    //    //[BsonRepresentation(BsonType.ObjectId)]
    //    public string _id { get; set; }

    //    [BsonElement("name")]
    //    public string name { get; set; }
    //}






}
