﻿using System.Fabric;
using System.Security;
using Microsoft.ServiceFabric.Actors.Runtime;
using MongoDB.Bson;
using MongoDB.Driver;

namespace ActorMongo
{
    public abstract class MongoDbProvider
    {
        /// <summary>
        /// 获取collection集合对象
        /// </summary>
        protected readonly IMongoCollection<BsonDocument> _Collection;

        public abstract string MongoDb { get;  }
        public abstract string CollecitonName { get; }
        private static string MongoUrl { get; set; }

        protected MongoDbProvider(ActorService actorService)
        {
            string _db = MongoDb;
            if (string.IsNullOrEmpty(MongoUrl))
                MongoUrl = GetConnectionPath(actorService);

            if (string.IsNullOrEmpty(MongoUrl))
                return;
            if (string.IsNullOrEmpty(_db))
                _db = "ServiceFabric";//设置默认值

            var db = new MongoClient(new MongoUrl(MongoUrl)).GetDatabase(_db);
            _Collection = db.GetCollection<BsonDocument>(CollecitonName);
        }

        //获取本actor中settings.xml中的mongo配置连接项
        private string GetConnectionPath(ActorService actorService)
        {
            var configurationPackage = actorService.Context.CodePackageActivationContext.GetConfigurationPackageObject("Config");
           return configurationPackage.Settings.Sections["MongodbConfig"].Parameters["ConnectionString"].Value;
        }
    }
    
}
