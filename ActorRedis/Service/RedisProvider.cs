﻿using System;
using Microsoft.ServiceFabric.Actors.Runtime;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Newtonsoft;

namespace ActorRedis.Service
{
    public abstract  class RedisProvider
    {
        private static StackExchangeRedisCacheClient redisContext;
        protected readonly IDatabase dbContext;
        private static ISerializer Ser;
        protected abstract int db { get; }
        private static string ConnectionStr=string.Empty;

        private static Lazy<ConnectionMultiplexer> lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
        {
            return ConnectionMultiplexer.Connect(ConnectionStr);

        });

        private static ConnectionMultiplexer redisConnection
        {
            get
            {
                return lazyConnection.Value;
            }
        }


        protected RedisProvider(ActorService service)
        {
            ConnectionStr = GetConnectionPath(service);

            dbContext = GetDb(db);
        }



        private static StackExchangeRedisCacheClient GetClient(int db)
        {
            Ser = new NewtonsoftSerializer();
            redisContext = new StackExchangeRedisCacheClient(redisConnection, Ser, db);
            return redisContext;
        }

        private static IDatabase GetDb(int db)
        {
            return redisConnection.GetDatabase(db);
        }


        private string GetConnectionPath(ActorService actorService)
        {
            var configurationPackage = actorService.Context.CodePackageActivationContext.GetConfigurationPackageObject("Config");
            return configurationPackage.Settings.Sections["RedisConfig"].Parameters["ConnectionString"].Value;
        }

    }
}
