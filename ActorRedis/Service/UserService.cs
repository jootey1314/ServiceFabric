﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ServiceFabric.Actors.Runtime;

namespace ActorRedis.Service
{
    public class UserService : RedisProvider
    {
        protected override int db => 0;

        public UserService(ActorService Service) : base(Service)
        {
        }
        public async Task<bool> addUser(string name)
        {
          return await dbContext.SetAddAsync("Users", name);
        }

}
}
