﻿using System;
using Microsoft.ServiceFabric.Actors;

namespace ObserverActor.Interfaces.Event
{
    public interface IGameEvents : IActorEvents
    {
        void GameScoreUpdated(Guid gameId, string currentScore);
    }
}
