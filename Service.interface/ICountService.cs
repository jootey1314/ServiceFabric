﻿using System.Threading.Tasks;
using Microsoft.ServiceFabric.Services.Remoting;

namespace Service.@interface
{
    public interface ICounter : IService
    {
        Task<long> GetCountAsync();
    }
}
