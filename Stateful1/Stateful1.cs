﻿using System;
using System.Collections.Generic;
using System.Fabric;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.ServiceFabric.Data.Collections;
using Microsoft.ServiceFabric.Services.Communication.Runtime;
using Microsoft.ServiceFabric.Services.Remoting.Runtime;
using Microsoft.ServiceFabric.Services.Runtime;
using Service.@interface;

namespace Stateful1
{
    /// <summary>
    /// 通过 Service Fabric 运行时为每个服务副本创建此类的一个实例。
    /// </summary>
    internal sealed class Stateful1 : StatefulService, ICounter
    {
        #region init


        /// <summary>
        ///可选择性地替代以创建侦听器(如 HTTP、服务远程、WCF 等)，从而使此服务副本可处理客户端或用户请求。
        /// </summary>
        /// <remarks>
        ///有关服务通信的详细信息，请参阅 https://aka.ms/servicefabricservicecommunication
        /// </remarks>
        /// <returns>侦听器集合。</returns>
        protected override IEnumerable<ServiceReplicaListener> CreateServiceReplicaListeners()
        {
            //return new ServiceReplicaListener[0];
            return new List<ServiceReplicaListener>(){new ServiceReplicaListener((context) =>this.CreateServiceRemotingListener(context))};
        }

        /// <summary>
        /// 这是服务副本的主入口点。
        /// 在此服务副本成为主服务并具有写状态时，将执行此方法。
        /// </summary>
        /// <param name="cancellationToken">已在 Service Fabric 需要关闭此服务副本时取消。</param>
        protected override async Task RunAsync(CancellationToken cancellationToken)
        {
            // TODO: 将以下示例代码替换为你自己的逻辑 
            //       或者在服务不需要此 RunAsync 重写时删除它。
            ConfigurationPackage configPackage = this.Context.CodePackageActivationContext.GetConfigurationPackageObject("Config");

            var myDictionary = await this.StateManager.GetOrAddAsync<IReliableDictionary<string, long>>("myDictionary");

            while (true)
            {
                cancellationToken.ThrowIfCancellationRequested();

                using (var tx = this.StateManager.CreateTransaction())
                {
                    var result = await myDictionary.TryGetValueAsync(tx, "Counter");

                    ServiceEventSource.Current.ServiceMessage(this.Context, "Current Counter Value: {0}",
                        result.HasValue ? result.Value.ToString() : "Value does not exist.");
                    await myDictionary.AddOrUpdateAsync(tx, "Counter", 0, (key, value) => ++value);

                    // 如果在调用 CommitAsync 前引发异常，则将终止事务，放弃 
                    // 所有更改，并且辅助副本中不保存任何内容。
                    await tx.CommitAsync();
                }

                await Task.Delay(TimeSpan.FromSeconds(1), cancellationToken);
            }
        }

        public Stateful1(StatefulServiceContext context)
            : base(context)
        { }

        #endregion

        public async Task<long> GetCountAsync()
        {
            var myDictionary = await this.StateManager.GetOrAddAsync<IReliableDictionary<string, long>>("myDictionary");

            using (var tx = this.StateManager.CreateTransaction())
            {
                var result = await myDictionary.TryGetValueAsync(tx, "Counter");
                return result.HasValue ? result.Value : 0;
            }


            //retry:
            //try
            //{
            //    // Create a new Transaction object for this partition
            //    using (ITransaction tx = base.StateManager.CreateTransaction())
            //    {
            //        // AddAsync takes key's write lock; if >4 secs, TimeoutException
            //        // Key & value put in temp dictionary (read your own writes),
            //        // serialized, redo/undo record is logged & sent to  
            //        // secondary replicas
            //        await m_dic.AddAsync(tx, key, value, cancellationToken);

            //        // CommitAsync sends Commit record to log & secondary replicas
            //        // After quorum responds, all locks released
            //        await tx.CommitAsync();
            //    }
            //    // If CommitAsync not called, Dispose sends Abort
            //    // record to log & all locks released
            //}
            //catch (TimeoutException)
            //{
            //    await Task.Delay(100, cancellationToken);
            //    goto retry;
            //}


            //using (ITransaction tx = StateManager.CreateTransaction())
            //{
            //    // Use the user’s name to look up their data
            //    ConditionalValue<User> currentUser =
            //       await m_dic.TryGetValueAsync(tx, name);

            //    // The user exists in the dictionary, update one of their properties.
            //    if (currentUser.HasValue)
            //    {
            //        // Create new user object with the same state as the current user object.
            //        // NOTE: This must be a deep copy; not a shallow copy. Specifically, only
            //        // immutable state can be shared by currentUser & updatedUser object graphs.
            //        User updatedUser = new User(currentUser);

            //        // In the new object, modify any properties you desire 
            //        updatedUser.LastLogin = DateTime.UtcNow;

            //        // Update the key’s value to the updateUser info
            //        await m_dic.SetValue(tx, name, updatedUser);

            //        await tx.CommitAsync();
            //    }
            //}


        }
    }
}
