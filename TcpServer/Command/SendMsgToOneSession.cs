﻿using System.Linq;
using SuperSocket;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace TcpServer.Command
{
    public class SendMsgToOneSession : CommandBase<TestSession, StringRequestInfo>
    {
        public override void ExecuteCommand(TestSession session, StringRequestInfo requestInfo)
        {
            var appServer = session.AppServer;
            var otherSessionId = requestInfo.Parameters.FirstOrDefault();
            var oneSession = appServer.GetSessionByID(otherSessionId);
            
            oneSession.Send(requestInfo.Parameters.LastOrDefault()+"customerId:"+ oneSession.CustomID??"");
        }
    }

}
