﻿using System;
using System.Linq;
using SuperSocket;
using SuperSocket.Common;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketEngine;
using SuperSocket.SocketBase.Protocol;

//SuperSocket.QuickStart.TelnetServer
namespace TcpServer
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Press any key to start the server!");

            //Console.ReadKey();
            //Console.WriteLine();

            var appServer = new TestServer();

            //Setup the appServer
            if (!appServer.Setup(2012)) //Setup with listening port
            {
                Console.WriteLine("Failed to setup!");
                Console.ReadKey();
                return;
            }
            appServer.NewSessionConnected += appServer_NewSessionConnected;
            appServer.SessionClosed += CloseSession;
            Console.WriteLine();
            //Try to start the appServer
            if (!appServer.Start())
            {
                Console.WriteLine("Failed to start!");
                Console.ReadKey();
                return;
            }
            Console.WriteLine("The server started successfully, press key 'q' to stop it!");
            while (Console.ReadKey().KeyChar != 'q')
            {
                Console.WriteLine();
                continue;
            }
            Console.WriteLine();
            //Stop the appServer
            appServer.Stop();

            Console.WriteLine("The server was stopped!");
        }

        static void appServer_NewSessionConnected(TestSession session)
        {
            session.CustomID = new Random().Next(10000, 99999);
            session.CustomName = "hello word";

            Console.WriteLine("the client " + session.SessionID.ToString() + "   is connected with the server");
            session.Send("Welcome to SuperSocket Telnet Server:" + session.SessionID.ToString());
        }

        static void CloseSession(TestSession session, CloseReason resion)
        {
            Console.WriteLine("the client " + session.SessionID.ToString() + " distroyed");
        }


    }











}
