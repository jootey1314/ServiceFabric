﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using Actor1.Interfaces;
using Actor2.Interfaces;
using ActorMongo.Interfaces;
using ActorRedis.Interfaces;
using Microsoft.ServiceFabric.Actors;
using Microsoft.ServiceFabric.Actors.Client;
using Microsoft.ServiceFabric.Services.Client;
using Microsoft.ServiceFabric.Services.Remoting.Client;
using ObserverActor.Interfaces;
using Service.@interface;

namespace WebApi1.Controllers
{


    public class GameEventsHandler : ObserverActor.Interfaces.Event.IGameEvents
    {
        public void GameScoreUpdated(Guid gameId, string currentScore)
        {
            Console.WriteLine(@"Updates: Game: {0}, Score: {1}", gameId, currentScore);
        }
    }



    [ServiceRequestActionFilter]
    public class ValuesController : ApiController
    {
        // GET api/values 
        public async Task<IEnumerable<string>> Get()
        {
            ICounter counter = ServiceProxy.Create<ICounter>(new Uri("fabric:/SeviceFabric/Stateful1"), new ServicePartitionKey(0));

            long count = await counter.GetCountAsync();

            return new string[] { count.ToString() };
        }

        public string getName()
        {
            return "ddddd";
        }


        [HttpGet]
        public async Task Subscribe()
        {
            IObserverActor proxy = ActorProxy.Create<IObserverActor>(new ActorId("jooper"), new Uri("fabric:/SeviceFabric/ObserverActorService"));
            var eventHandle = new GameEventsHandler();
            await proxy.SubscribeAsync<ObserverActor.Interfaces.Event.IGameEvents>(eventHandle);
        }


        [HttpGet]
        //[Route("notify")]
        public async Task Notify(int id)
        {
            //long partitionKey = ((Int64RangePartitionInformation)partition.PartitionInformation).LowKey;
            await Subscribe();
            IObserverActor proxy = ActorProxy.Create<IObserverActor>(new ActorId("jooper"), new Uri("fabric:/SeviceFabric/ObserverActorService"));
            //await proxy.UpdateGameStatus("dddd");
            var Result = await proxy.GetGameScore();
        }

        [HttpGet]
        public async Task<string> AddMongoUser(string name)
        {
            //bool isWait(string s) => true;var ddd= isWait("dddd");
            var proxy = ActorProxy.Create<IActorMongo>(ActorId.CreateRandom(),new Uri("fabric:/SeviceFabric/ActorMongoActorService"));
            return await proxy.AddAccountAsync(name);
        }

        [HttpGet]
        public async Task<dynamic> GetMongonUser(string name)
        {
            var proxy = ActorProxy.Create<IActorMongo>(ActorId.CreateRandom(), new Uri("fabric:/SeviceFabric/ActorMongoActorService"));
            var result= await proxy.GetAccountByNameAsync(name);
            //var content = new StringContent(result, System.Text.Encoding.UTF8, "application/json");
            //return new HttpResponseMessage { Content = content };
            return result;
        }





        [HttpGet]
        public async Task<bool> AddRedisUser(string name)
        {
            //bool isWait(string s) => true;var ddd= isWait("dddd");
            var proxy = ActorProxy.Create<IActorRedis>(ActorId.CreateRandom(), new Uri("fabric:/SeviceFabric/ActorRedisActorService"));
            return await proxy.addUser(name);
        }


        [HttpGet]
        public async Task<string> GetSf2()
        {
            var proxy = ActorProxy.Create<IActor2>(ActorId.CreateRandom(), new Uri("fabric:/ServiceFabric2/Actor2ActorService"));
            return await proxy.Get();
        }





        // GET api/values/5 
        public string Get(int id)
        {
            System.Diagnostics.Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start(); //  开始监视代码运行时间  
            //  you code ....  

            IActor1 actor = ActorProxy.Create<IActor1>(ActorId.CreateRandom(),new Uri("fabric:/SeviceFabric/Actor1ActorService"));

            CancellationToken cancel = new CancellationToken();
           //var result= actor.GetCountAsync(cancel).Result;
            var result = actor.GetPatitionId(cancel).Result;


            stopwatch.Stop(); //  停止监视  
            TimeSpan timespan = stopwatch.Elapsed; //  获取当前实例测量得出的总时间  
            double milliseconds = timespan.TotalMilliseconds;


            return "Service Fabrice Actor：PatitionId:" + result.ToString()+",总共耗时："+milliseconds.ToString()+"毫秒";
        }

        // POST api/values 
        public void Post([FromBody]string value)
        {
        }

        //输入 API/值/5
        public void Put(int id, [FromBody]string value)
        {
           
        }

        // DELETE api/values/5 
        public  void Delete(int id)
        {

        }


    }
}
