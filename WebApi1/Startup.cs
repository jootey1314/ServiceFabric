﻿using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using Newtonsoft.Json.Serialization;
using Owin;



namespace WebApi1
{
    public static class Startup
    {
        // 此代码会配置 Web API。启动类指定为
        // WebApp.Start 方法中的类型参数。
        public static void ConfigureApp(IAppBuilder appBuilder)
        {
            // 配置自托管的 Web API。 
            HttpConfiguration config = new HttpConfiguration();





            // Web API configuration and services
            var json = config.Formatters.JsonFormatter;
            // 解决json序列化时的循环引用问题
            json.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            // 干掉XML序列化器
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();



            // Web API routes
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new {id = RouteParameter.Optional}
            //);

            config.Routes.MapHttpRoute(
                name: "ActionApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new {id = RouteParameter.Optional}
            );
            appBuilder.UseWebApi(config);
        }


        //public static IAppBuilder UseMvcWithDefaultRoute(this IAppBuilder app)
        //{
        //    if (app == null)
        //        throw new ArgumentNullException("app");
        //    return app.UseWebApi(routes => routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}")));
        //}

    }
}
