/// args
var target = Argument("target", "default");


/// build task
Task("build")
    .Does(() =>
{
    MSBuild("./SeviceFabric.sln", new MSBuildSettings{
        Verbosity = Verbosity.Minimal
    });
});


Task("default")
    .IsDependentOn("build");


 /// nuget task
 Task("restore-nuget-packages")
     .Does(() =>
 {
     NuGetRestore("./SeviceFabric.sln");
 });

Task("clean")
     .Does(() =>
 {
    //  CleanDirectories("./src/*/bin");
    //  CleanDirectories("./test/*/bin");
 });


/// unit-test task
Task("unit-test")
    .IsDependentOn("build")
    .Does(() =>
{
    // XUnit2("./test/*/bin/*/*.Tests.dll");
});

/// run task
RunTarget(target);